import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
    void getName() {
        Weapon w = new Weapon("Flatline", 20);
        //test the name to see if it returns as expected
        assertEquals(w.getName(), "Flatline");
    }

    @Test
    void getDamage() {
        Weapon w = new Weapon("Flatline", 20);
        //test the number to see if it matches what is entered
        assertEquals(w.getDamage(), 20);
    }

    @Test
    void setName() {
        Weapon w = new Weapon("Flatline", 20);
        //test to make sure the set method sets as expected
        w.setName("Surge Rifle");
        assertEquals(w.getName(), "Surge Rifle");
    }

    @Test
    void setDamage() {
        Weapon w = new Weapon("Flatline", 20);
        //test to see if a change in damage goes through
        w.setDamage(22);
        assertEquals(w.getDamage(), 22);
    }
}