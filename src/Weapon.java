
public class Weapon {
    //initializers
    private String name;
    private int damage;

    //constructor
    public Weapon(String n, int d) {
        this.name=n;
        this.damage=d;
    }
    //getter and setter methods for the two variables
    public void setName(String n) {this.name=n;}
    public void setDamage(int d) {this.damage=d;}
    public String getName() {return name;}
    public int getDamage() {return damage;}
}
