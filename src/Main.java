import java.util.*;

public class Main {
    public static void main(String[] args) {

        //starting variables
        Scanner keyboard = new Scanner(System.in);
        String wName;
        int dmg;
        int i;
        char response;
        ArrayList<Weapon> weapons = new ArrayList<Weapon>();

        //this program will create objects of weapons and their damage output indefinitely
        System.out.println("This program will let you create up to 5 fantasy weapons and their damage values.");
        for (i=0; i<5; i++) {
            //prompt the user to create or search for a weapon they created
            System.out.println("(c)reate weapons, (l)ist weapons or (e)xit?: ");
            response = keyboard.next().charAt(0); //retrieve only the first character entered
            switch (response) {

                case 'c': //if the user chooses to create a weapon
                    try {
                        //request variables NAME and DAMAGE
                        System.out.println("Please enter a name for weapon " + (i + 1) + ": ");
                        wName = keyboard.next();
                        System.out.println("How much damage does this weapon do? Integer please: ");
                        dmg = keyboard.nextInt();

                        //create the object using the variables
                        System.out.println("Creating...");
                        Weapon sword = new Weapon(wName, dmg); //creating object to store values
                        sword.setName(wName);
                        sword.setDamage(dmg);
                        weapons.add(sword); //add object to weapons ArrayList
                        System.out.println("Created " + sword.getName() + " and added to list.");
                        keyboard.nextLine();
                    } catch (InputMismatchException noNumber) { //if someone types an unexpected value
                        System.out.println("Not a valid entry as a weapon.");
                        keyboard.nextLine();
                        i--;
                    }
                    break;

                case 'l': //if the user chooses to list the available weapons
                    i=0;
                    for (i=0; i<weapons.size(); i++) {
                        System.out.println((weapons.get(i)).getName()+" , "+(weapons.get(i).getDamage()));
                    }
                    System.exit(4);
                    break;

                case'e': //if the user chooses to exit
                    System.exit(4);
                    break;

                default: //if the user enters an invalid option
                    System.out.println("Please type c, l, or e.");
                    keyboard.nextLine();
            }



        }
    }
}

/*
    public static void main(String[] args) {
        //write starting variables
        Scanner keyboard = new Scanner(System.in);
        int carrier = 0;
        int SIZE = 5;
        int i = 0;
        int[] mathArray = {0, 0, 0, 0, 0};
        //this program will add, subtract, multiply, then divide using 4 whole numbers
        System.out.println("This program will add, subtract, multiply, then divide your numbers, in that order.");
        while (i < SIZE) {
            switch (i) {
                case 0: //prompt user for first number
                    System.out.println("Please enter a starting number: ");
                    //store into array
                    carrier = keyboard.nextInt();
                    break;

                case 1: //prompt user for addition number
                    System.out.println("Now we will add: ");
                    carrier = keyboard.nextInt();
                    break;

                case 2: //subtraction number
                    System.out.println("Next we subtract: ");
                    carrier = keyboard.nextInt();
                    break;

                case 3: //multiplication number
                    System.out.println("Multiply the sum by: ");
                    carrier = keyboard.nextInt();
                    break;

                case 4: //division number
                    System.out.println("Then Divide all of that by: ");
                    carrier = keyboard.nextInt();
                    break;
            }
            mathArray[i] = carrier;
            i++;
        }
        //now that the array is completed, time to math
        System.out.println("Time to math.");
        int result;
        //addition first
        System.out.println(mathArray[0] + " + " + mathArray[1] + " is " + mathArray[0] + mathArray[1] + ".");
        result = mathArray[0] + mathArray[1];
        //subtraction
        System.out.println(result + " - " + mathArray[2] + " equals " + (result - mathArray[2]) + ".");
        result = result - mathArray[2];
        //multiplication
        System.out.println(result + " times " + mathArray[3] + " amounts to " + (result * mathArray[3]) + ".");
        result = result * mathArray[3];
        //division
        System.out.println(result + " divided by " + mathArray[4] + " is finally " + (result / mathArray[4]) + "!");
        result = result / mathArray[4];
        //ERROR CLEAR keyboard.nextLine();
        //jUnit will check if numbers 1 and 2 result in 0
        //  (which will make the multiplication 0 and then divide by 0)
    }
*/